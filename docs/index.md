---
title : "DCAT Validation"
description: "Documentation for the DCAT validator"
lead: "Validator that checks a given DCAT file conforms to an application profile."
date: 2023-08-14T14:39:12+02:00
draft: false
toc: true
---

*You can find the repository on [GitLab](https://gitlab.com/digilab.overheid.nl/ecosystem/dcat-v2-validator).*

## DCAT
For the Dutch Government's Federatief Datastelsel, datasets and services should be documented in a uniform way. To do this [dcat-v2](https://www.w3.org/TR/vocab-dcat-2/) will be used.  
Dcat is a [RDF](https://www.w3.org/RDF/) based format, a standard for specifying structured and linked data, that forms a directed graph.

## SHACL
SHACL is a language for validation of RDF graphs, it allows specifying conditions that a RDF graph should meet. 


## Application profiles
Application profiles further narrow the dcat specification for use in specific use cases:

- The application profiles for sharing datasets in the eu: [DCAT-AP-EU](https://joinup.ec.europa.eu/collection/semic-support-centre/solution/dcat-application-profile-data-portals-europe/releases)
- The application profile for sharing data in the netherlands, made by data.overheid.nl [DCAT-AP-DONL](https://docs.datacommunities.nl/data-overheid-nl-documentatie/dcat/dcat-ap-donl)
- The application profile for sharing data in the netherlands, currently in development [DCAT2-AP-NL](https://geonovum.github.io/dcat2-ap-nl/)

DCAT-AP-EU already provides SHACL shape files for validation.  
DCAT2-AP-NL has an open issue for adding SHACL support [here](https://github.com/Geonovum/dcat2-ap-nl/issues/1).

## Libraries

The SHACL standard includes a list of libraries that may be used for RDF validation using SHACL [here](https://w3c.github.io/data-shapes/data-shapes-test-suite/#validate-rdf-data-tests)

## CLI
A cli tool has been made to make SHACL validation locally or in CI/CD easier, releases are available [here](https://gitlab.com/digilab.overheid.nl/ecosystem/dcat-v2-validator/shacl-validator/-/releases).
